## Synopsis

A small Ruby program that plays the game [Duck, Duck, Goose](https://en.wikipedia.org/wiki/Duck,_duck,_goose "Wikipedia""). Requested as part of a technical interview to demonstrate my approach to object-oriented design.

###Requirements

* Use Ruby to implement the game Duck, Duck, Goose.
* There is no need to implement automated test cases.

### Design

The rules for DDG (Duck, Duck, Goose) are very simple. From Wikipedia:

> A group of players sit in a circle, facing inward, while another player, who is "it", walks around tapping or pointing to each player in turn, calling each a "duck" until finally calling one a "goose" (or a "gray duck" in Minnesota). The "goose" then rises and tries to tag "it", while "it" tries to return to and sit where the "goose" had been sitting. If "it" succeeds, the "goose" becomes "it" and the process begins again. If the "goose" tags "it", the "goose" may return to sit in the previous spot and "it" resumes the process.

#### Assumptions

* The game requires at least 4 players.
* For _n_ number of players there are _n - 1_ chairs to sit in.
* Each player can move at a different speed.

### Usage

To run the program:

	ruby duck_duck_goose.rb

Enter the desired number of players (must be 4 or higher) and the game will start. Type 'yes' or enter a blank line when prompted to continue the game.
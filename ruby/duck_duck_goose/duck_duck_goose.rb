require File.expand_path(File.dirname(__FILE__) + '/player')
require File.expand_path(File.dirname(__FILE__) + '/playing_area')

class DuckDuckGoose

  # Constants used for input validation
  MINIMUM_NUMBER_OF_PLAYERS = 4
  MAX_PLAYERS_IF_UNSPECIFIED = 25


  # Initialization.
  # An explicit number of players can be specified. Otherwise, a random number of players will be chosen.
  def initialize(number_of_players = nil)
    # Set up the number of players and rounds, choosing a random value if unspecified.
    number_of_players = number_of_players.nil? ? Random.new.rand(MINIMUM_NUMBER_OF_PLAYERS..MAX_PLAYERS_IF_UNSPECIFIED) : number_of_players.to_i

    # Input validation
    raise ArgumentError, "There must be at least #{MINIMUM_NUMBER_OF_PLAYERS} players!" if number_of_players < MINIMUM_NUMBER_OF_PLAYERS

    # Set up the chairs
    @playing_area = PlayingArea.new(number_of_players - 1)

    # Create the seated players
    for i in 1..@playing_area.number_of_chairs
      @playing_area.seat(Player.new("Player #{i}"))
    end

    # Create the unseated ('it') player
    @it = Player.new("Player #{number_of_players}")
  end

  # Starts a round of play
  def play_a_round
    puts "Starting a round (#{@it} is It)..."

    # Select the player that will be tagged as the Goose
    goose_index = Random.new.rand(0...@playing_area.number_of_chairs)
    for i in 1..goose_index do puts 'Duck...' end

    # Start the goose
    goose = @playing_area.unseat(goose_index)
    puts "GOOSE! #{goose} is the Goose!"

    # Simulate the run.
    caught = goose.chase(@it, @playing_area.circumference, 1)
    if caught
      puts "#{goose} tagged #{@it}!"
      @playing_area.seat(goose)
    else
      puts "#{@it} made it to the seat! #{goose} is It now."
      @playing_area.seat(@it)
      @it = goose
    end
  end

  def number_of_players
    @playing_area.number_of_chairs + 1
  end

  # Object output
  def to_s
    'Duck, Duck, Goose'
  end

  def inspect
    <<EOS
There are #{number_of_players} players.
#{@it} is It.
#{@playing_area.inspect}
EOS
  end
end

END {
  # Startup banner
  puts '####################'
  puts '# DUCK DUCK GOOSE! #'
  puts '# By Rob Schafer   #'
  puts '####################'
  puts
  
  # Gather input
  game = nil
  until !game.nil? do
    puts 'Enter the number of players:'
    begin
      input = gets
      game = DuckDuckGoose.new(input.chomp)
    rescue Interrupt
      exit
    rescue Exception => e
      puts e.message
    end
  end

  puts 'Starting Duck, Duck, Goose!'
  puts 'Initial state:'
  puts game.inspect
  
  done = false
  until done do
    game.play_a_round
    puts "Play another round? (enter 'yes' or a blank line to continue)"

    begin
      input = gets.chomp.downcase
      done = !input.eql?('yes') && !input.eql?('')
    rescue Interrupt
      exit
    end
  end

  puts 'Final state:'
  puts game.inspect
  puts '# THANKS FOR PLAYING! #'
}
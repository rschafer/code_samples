class PlayingArea

  MIN_NUMBER_OF_CHAIRS = 3
  CHAIR_SPACING = 60  # distance between chairs in cm (~2 ft)

  def initialize(number_of_chairs)
    number_of_chairs = number_of_chairs.to_i

    # Input validation
    raise ArgumentError, "The playing area must contain at least #{MIN_NUMBER_OF_CHAIRS} chairs!" if number_of_chairs < MIN_NUMBER_OF_CHAIRS

    @chairs = Array.new(number_of_chairs)
  end

  def number_of_chairs
    @chairs.count
  end

  # Assigns a player to an empty seat if one exists.
  # Returns the Player that was assigned.
  def seat(player)
    return nil if player.nil?

    i = @chairs.find_index(nil)
    raise IndexError, "There are no empty seats left!" if i.nil?

    @chairs[i] = player

    player
  end

  # Removes a player from a particular seat.
  # Returns the Player object that was in the seat.
  def unseat(chair_number)
    # Input validation
    raise IndexError, "#{chair_number} is not a valid chair index!" unless (0..number_of_chairs).include?(chair_number)

    player = @chairs[chair_number]
    @chairs[chair_number] = nil
    player
  end

  # Returns the length of a circle containing the playing area (in cm.)
  # This is technically called the circumradius
  def circumference
    # Via http://www.mathopenref.com/polygonradius.html
    # Note that the formula assumes the sin method is working with degrees whereas Math.sin takes an argument in radians
    radius = CHAIR_SPACING / (2.0 * Math.sin((180.0 / number_of_chairs) * (Math::PI / 180.0)))

    # Add some spacing since players don't run on top of the chairs
    radius = radius + CHAIR_SPACING

    2.0 * Math::PI * radius
  end

  # Object output
  def inspect
    @chairs.collect{ |player| "Chair ##{@chairs.index(player) + 1}: #{player}"}.join("\n")
  end
end
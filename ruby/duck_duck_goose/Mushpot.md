Some rules (["Go Out And Play!: Favorite Outdoor Games From Kaboom!](https://books.google.com/books?id=Bi_naA0c5YcC&pg=PA58&dq=drip+drip+drop+goose&hl=en&sa=X&ved=0ahUKEwj3vq-pmMDOAhVHbR4KHV2RCS8Q6AEIIjAB#v=onepage&q=drip%20drip%20drop%20goose&f=false "Google Books")) allow for sending the *it* player to the center of the circle when it is tagged by the *goose*. In this case the *goose* then becomes *it* and the player in the center must stay there until another *goose* succeeds in tagging *it*, thereby releasing the player from the center to the seat vacated by the *goose* (which is now *it*.) This prevents a condition arising where an especially slow player is never able to evade a pursuing "goose" and must thereby take a large number of turns as "It."

> Choose one player to be "It." The rest of the players sit in a circle facing each other. "It" walks or skips around the outside of the circle, gently tapping each player's head in turn, calling out "Duck!" with each tap.
> Eventually "It" taps one of the players' heads and calls "Goose!" The player who was tapped gets up and chases "It" around the circle. "It" tries to make it back to the tapped player's seat before getting tagged

> If "It" does make it back, the goose becomes "It" for the next round. If "It" gets tagged, he or she has to sit in the middle of the circle. The goose then becomes "It" for the next round.

> Meanwhile, the player in the middle can't leave until another player is tagged and takes his or her place.

# An individual player.
class Player
  BASE_RUNNING_SPEED = 447.04 # In cm/s, ~10MPH per https://www.reference.com/health/average-human-running-speed-7f0ef0953669fa1#

  def initialize(name=nil)
    @name = name
    @quickness = Random.new.rand(0.5..1.5)
  end

  # Returns speed of the player in cm/s.
  def speed
    @quickness * BASE_RUNNING_SPEED
  end

  # Returns true if this player can catch the other over the specified distance given a headstart.
  def chase(other_player, distance, headstart = 1)
    # How long will it take the other player to reach the end?
    run_time = distance / other_player.speed
    
    # How far will this player get in the same period? Account for the headstart.
    distance_covered = (run_time - headstart) * self.speed
    
    # If this player covers more distance than the other player they will overtake
    distance_covered > distance
  end

  def to_s
    @name.nil? ? "Player #{self.object_id}" : @name
  end

  def inspect
    "#{self} (speed=#{self.speed} cm/s)"
  end
end